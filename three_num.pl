#!/usr/bin/perl
use strict;
use warnings;
print add_three_numbers( 12, 592, 2928) ;
print add_three_numbers( 9, 2, 4, 5, 10);
sub add_three_numbers{
	my (@nums) = @_;
	if (scalar(@nums) <3)
	{	
		die "Not enougb parameters.";
	}
	elsif (scalar(@nums) >3)
	{
		die "Too many parameters.";
	}
	else
	{
		return $nums[0]+$nums[1]+$nums[2];
	}
}
